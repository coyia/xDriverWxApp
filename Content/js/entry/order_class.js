(function($, doc) {	
	$.init({

    });

    $.ready(function () {
    		var classid=0;
			if(UrlParm.hasParm("classid"))
			{
				classid=UrlParm.parmValues("classid")[0];
			}
			Entry.GetInitClassData(classid,function(data) {
				if(data.status) {
					
					
					document.getElementById("Show_Name").innerHTML=data.data.Name;
					document.getElementById("Show_Price").innerHTML=data.data.IfInstalment==0?"￥"+data.data.Price:"首付￥"+data.data.Price;
					document.getElementById("Show_SalesPrice").innerHTML="￥"+data.data.SalesPrice;
					document.getElementById("Show_TrainByCar").innerHTML=data.data.TrainByCar==1?"一人一车":data.data.TrainByCar==2?"四人一车":"多人一车";
					document.getElementById("Show_TrainDay").innerHTML=data.data.TrainDay;
					document.getElementById("Show_IfBusService").innerHTML=data.data.IfBusService==0?"不支持":"支持";
					document.getElementById("Show_IfInstalment").innerHTML=data.data.IfInstalment==0?"不支持":"支持";
					
					document.getElementById("Show_DrvLicense").innerHTML=data.data.DrvLicense;
					document.getElementById("Show_RegDTM").innerHTML=data.data.BeginDTM+" - " +data.data.EndDTM;
				
				} else {
					mui.alert(data.message, null);
				}
				
			},
			function(data) {
				mui.alert('服务器维护中，请稍后再试！', null);
			});
			

			var Btn_NextStep = document.getElementById('a_NextStep');
            Btn_NextStep.addEventListener('tap', function (event) {
                   window.location.href = "order_place.html?classid="+classid+"&r=" +Math.random();
            });
    });
	
}(mui, document));  