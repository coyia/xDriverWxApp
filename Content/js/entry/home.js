(function($, doc) {	
	$.ready(function(){

		/*初始化*/
		Entry.GetHome(function(data){
			if(data.status)
			{	
				var inidata=data.data; 

				cache('areacode',inidata.AreaCode);
				cache('unionid',inidata.unionid);
				

			var Btn_More = document.getElementById('a_more');
	            Btn_More.addEventListener('tap', function (event) {
	            	getClass(0);
	            	this.style.display='none';
	            });
			//获取驾校基本信息
			Entry.GetInitInfo(function(data) {
					if(data.status) {
						var entryInfo=data.data;
					$(".inst").each(function() {
						if (this.id == "ImageUrlArgs") {
							//图片轮播
							
							var imgs=entryInfo[this.id];
							var imgCount=imgs['Count']
							this.innerHTML="";
							
							var imgHtml='';
							//前面多增加一张轮播不会出现闪动的样式
							imgHtml+='<div class="mui-slider-item">';
							imgHtml+='<a href="#">';
							imgHtml+='<img src="'+imgs["Host"]+imgs["UrlRows"][0]["Url"]+'"></a></div>';
							for(var i=0;i<imgCount;i++){
							 	imgHtml+='<div class="mui-slider-item">';
								imgHtml+='<a href="#">';
								imgHtml+='<img src="'+imgs["Host"]+imgs["UrlRows"][i]["Url"]+'"></a></div>';
							}
							//最后面多增加一张轮播不会出现闪动的样式
							imgHtml+='<div class="mui-slider-item">';
							imgHtml+='<a href="#">';
							imgHtml+='<img src="'+imgs["Host"]+imgs["UrlRows"][0]["Url"]+'"></a></div>';
							
						this.innerHTML = imgHtml;	
						
						//启动图片轮播
						var slider = mui("#slider");		 
						slider.slider({	interval: 2000	});		
						
	
						document.getElementsByClassName("page-count")[0].innerHTML=imgCount;	
						document.querySelector('#slider').addEventListener('slide', function(event) {					
							var current_num = event.detail.slideNumber;　　　	
							document.getElementsByClassName("page-index")[0].innerHTML=current_num + 1; 					
						});
	
					
						} else if (this.id == "LogoImgUrl") {
							//logo
							this.src= entryInfo[this.id];
						} else if(this.id == "ContactTel"){
							//电话
							this.href="tel:"+entryInfo[this.id];
							document.getElementById('ContactTelMore').href="tel:"+entryInfo[this.id];
						} else if (entryInfo[this.id] != undefined && entryInfo[this.id] != null) {
							this.innerHTML = entryInfo[this.id];
						}
					});
				
				
				
					} else {
						mui.alert(data.message, null);
					}
				},
				function(data) {
					mui.alert('服务器维护中，请稍后再试！', null);
				});
				
				//加载班型列表
				getClass(2);
			
			} else {
				mui.alert(data.message, null);
			}
		},function(data){
			mui.alert('服务器维护中，请稍后再试！', null);
		});
			
	});
			
		
	/*获取班型列表,classCount=0 表示获取所有,其他值则表示返回的数量*/
function getClass(take){
	Entry.GetInitClass(take,function(data){
		if(data.status)
		{
			var cdata=data.data;
			var clsList=document.getElementsByClassName("classList")[0];
			clsList.innerHTML='';
			var clshtml='';
			for(var i=0;i<cdata.length;i++)
			{
				clshtml+='<li><dl><dd><div class="class-name"><span>'+cdata[i].Name+'</span> <span>C1</span></div>';
				clshtml+='<div class="class-describe"><span>'+cdata[i].TrainByCar+'人/车 </span> <span>'
				var IfBusService=cdata[i].IfBusService==0?'自行前往':'班车接送'
				clshtml+=IfBusService+'</span> <span>'+cdata[i].TrainDay + '</span></div><div class="price"><span class="present">￥'+cdata[i].SalesPrice+'</span> '
				var IfInstalment=cdata[i].IfInstalment==1?' <span class="deposit">首付<i>￥200</i></span>':'';
				clshtml+=IfInstalment +'</div></dd><dd><a href="order_class.html?classid='+cdata[i].ClassId+'" class="mui-btn mui-btn-primary">报名</a></dd></dl></li>';
			}
			
			clsList.innerHTML +=clshtml;
		} else {
			mui.alert(data.message, null);
		}
	},function(data){
		mui.alert('服务器维护中，请稍后再试！', null);
	});
	
}		
			
}(mui, document));  


