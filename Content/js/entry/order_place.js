(function($, doc) {	
	$.init({

    });

    $.ready(function () {
            var classid=0;
            var ReadData=[];
            
			if(UrlParm.hasParm("classid"))
			{
				classid=UrlParm.parmValues("classid")[0];
			}
			Entry.GetInstByPlaceList(function(data) {
				if(data.status) {
					ReadData=data.data;
					var HTMLListStr="";
					if(data.data.length==1)
					{
						HTMLListStr="<li>"+
								"<div class='mui-checkbox'><input name='PlaceCHK_"+data.data[0].inst.PlaceId+"' id='PlaceCHK_"+data.data[0].inst.PlaceId+"' type='checkbox' checked='true'></div>"+					
								"<a class='' href='#'>"+									
									"<dl>"+
										"<dt>"+data.data[0].inst.Name+"</dt>"+
										"<dd>"+data.data[0].inst.Address+"</dd>"+
									"</dl>"+
								"</a>"+
							"</li>";
					}
					else
					{
						for (var i=0;i<data.data.length;i++) {
							HTMLListStr+="<li>"+
								"<div class='mui-checkbox'><input name='PlaceCHK_"+data.data[i].inst.PlaceId+"' id='PlaceCHK_"+data.data[i].inst.PlaceId+"' type='checkbox' ></div>"+					
								"<a class='' href='#'>"+									
									"<dl>"+
										"<dt>"+data.data[i].inst.Name+"</dt>"+
										"<dd>"+data.data[i].inst.Address+"</dd>"+
									"</dl>"+
								"</a>"+
							"</li>";
						}	
					}
			
					document.getElementById("PlaceList").innerHTML=HTMLListStr;
				} else {
					mui.alert(data.message, null);
				}
			},
			function(data) {
				mui.alert('服务器维护中，请稍后再试！', null);
			});
			
			var Btn_NextStep = document.getElementById('a_NextStep');
            Btn_NextStep.addEventListener('tap', function (event) {
                    if(ReadData.length==0)
                    {
                    	mui.alert("获取训练场失败");
                    }
                    else
                    {
                    	var PlaceIDs="";
                    	for (var i=0;i<ReadData.length;i++) {
                    		var checked=document.getElementById("PlaceCHK_"+ReadData[i].inst.PlaceId).checked;
                    		if(checked)
                    		{
                    			PlaceIDs+=ReadData[i].inst.PlaceId+",";
                    		}
                    	}	
                    	if(PlaceIDs=="")
                    	{
                    		mui.alert("请选择训练场");
                    	}
                    	else
                    	{
                    		PlaceIDs=PlaceIDs.substr(0,PlaceIDs.length-1);
                    		 window.location.href = "order_reg.html?classid="+classid+"&Placeids="+PlaceIDs+"&r=" +Math.random();
                    	
                    	}
                    }
            });
    });
	
}(mui, document));  