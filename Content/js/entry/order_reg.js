(function($, doc) {	
	$.init({

    });

    $.ready(function () {
            var classid=0;
			if(UrlParm.hasParm("classid"))
			{
				classid=UrlParm.parmValues("classid")[0];
			}
 			var Placeids="";
			if(UrlParm.hasParm("Placeids"))
			{
				Placeids=UrlParm.parmValues("Placeids")[0];
			}

			var btn_getCode = document.getElementById('btn_getCode');
            btn_getCode.addEventListener('tap', function (event) {
                var CellPhone=document.getElementById("txt_MobilePhone").value;
            	if(CellPhone=="")
            	{
            		mui.alert("请输入手机号");
            	}
            	else{sendSMS(CellPhone);}
            	
            });
            
            
             var btn_Agree = document.getElementById('txt_Agree');
            btn_Agree.addEventListener('tap', function (event) {
            	if(!this.checked)
            	{
            		document.getElementById("a_NextStep").className="mui-btn  mui-btn-primary mui-btn-block";
            		
            	}
            	else
            	{
            		document.getElementById("a_NextStep").className="mui-btn  mui-btn-primary mui-btn-block mui-disabled";
            	}
            });
            
            var btn_NextStep = document.getElementById('a_NextStep');
            btn_NextStep.addEventListener('tap', function (event) {
                var CellPhone=document.getElementById("txt_MobilePhone").value;
                var RealName=document.getElementById("txt_RealName").value;
            	var IDCard=document.getElementById("txt_IDCard").value;
            	var MobilePhone=document.getElementById("txt_MobilePhone").value;
            	var cpCode=document.getElementById("txt_cpCode").value;
            	var CarNO=document.getElementById("txt_CarNO").value;
            	var RefereeMan=document.getElementById("txt_RefereeMan").value;
            	var Agree=document.getElementById("txt_Agree").checked;
            	if(RealName=="")
            	{
            		mui.alert("请输入姓名");
            		return;
            	}
            	if(IDCard=="")
            	{
            		mui.alert("请输入身份证号");
            		return;
            	}
            	if(MobilePhone=="")
            	{
            		mui.alert("请输入手机号");
            		return;
            	}
            	if(cpCode=="")
            	{
            		mui.alert("请输入验证码");
            		return;
            	}

            	if(Agree)
            	{
		            	var params = {};
						params.ClassId=classid;
		                params.PlaceIdArgs=Placeids;
		                params.UserName= RealName;
		                params.IDCard=IDCard;
		                params.TelPhone=MobilePhone;
		                params.CarNO=CarNO;
		                params.RefereeMan=RefereeMan;
						params.Code=cpCode;
						Entry.OrderCreate(params,function(data) {
							if(data.status) {
								 window.location.href = "order_confirm.html?OrderNO="+data.OrderNO+"&r=" +Math.random();
							} else {
								mui.alert(data.message, null);
							}
						},
						function(data) {
							mui.alert('服务器维护中，请稍后再试！', null);
						});
		        }
            });
    });
	
	
	
	//发送短信
	function sendSMS(CellPhone) {
		Entry.smsSend(CellPhone,
				function(data) {
					if(data.status) {
					  btnSMSTimeStep(90);
					} else {
						mui.alert(data.message, null);
					}
				},
				function(data) {
					mui.alert('服务器维护中，请稍后再试！', null);
				});
	}
	
	var smsTime;
	var curTicks = 0;
	function btnSMSTimeStep(ticks) {
		if(curTicks<= 0)
		{
		    curTicks = ticks;
		    smsTime = setInterval(function () {
		        if (curTicks-- <= 0) {
		            //结束按钮状态还原
		            document.getElementById("btn_getCode").innerHTML='发送短信验证码';
		            clearInterval(smsTime);
		        }
		        else {
		            //倒计时
		            document.getElementById("btn_getCode").innerHTML='重新发送（' + curTicks + '）';
		
		        }
		    }, 1000);
	    }
	}
}(mui, document));  


