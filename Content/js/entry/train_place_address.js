(function($, doc) {	
	$.init({

    });

    $.ready(function () {
			Entry.GetInitInfo(function(data) {
				if(data.status) {
					var xPoint=data.data.LonLat.split(',')[0];
					var yPoint=data.data.LonLat.split(',')[1];
					addMarker(xPoint,yPoint,data.data.InstName);
				} else {
					mui.alert(data.message, null);
				}
			},
			function(data) {
				mui.alert('服务器维护中，请稍后再试！', null);
			});
			

    });
    
    
     //添加marker标记
    function addMarker(xPoint,yPoint,InstName) {
    		//地图初始化时，在地图上添加一个marker标记,鼠标点击marker可弹出自定义的信息窗体
	    var map = new AMap.Map("container", {
	        resizeEnable: true,
	        center: [xPoint,yPoint],
	        zoom: 13
	    });  
        map.clearMap();
        var marker = new AMap.Marker({
            map: map,
            position: [xPoint, yPoint]
        });
       
       var infoWindow = new AMap.InfoWindow({
	        isCustom: true,  //使用自定义窗体
	        content: createInfoWindow(InstName),
	        offset: new AMap.Pixel(25, -35)
   	   });;
       infoWindow.open(map, marker.getPosition());
    }

   

    //构建自定义信息窗体
    function createInfoWindow(InstName) {
        var info = document.createElement("div");
        info.className = "info";

        // 定义顶部标题
        var top = document.createElement("div");
        var titleD = document.createElement("div");
        top.className = "info-top";
        titleD.innerHTML = InstName;
        top.appendChild(titleD);
        info.appendChild(top);
        return info;
    }
	
}(mui, document));  