var HostUrl = function(str) {
	HOST = str;
}

var animationIn = 'pop-in';
var animationOut = 'pop-out';
//var baseUrl = 'https://5car.top/api/Mobile/';
var baseUrl = 'http://jx.weixin.zzxc.org.cn/api/';

var apiEntry ={
	GetHome:baseUrl+'Entry/GetHome',   //717000001
	GetInitInfo:baseUrl+'Entry/GetInitInfo',   //717000001
	GetInitClass:baseUrl+'Entry/GetInitClass',
	GetInstByPlaceList:baseUrl+'Entry/GetInstByPlaceList',
	GetInstByPlaceId:baseUrl+'Entry/GetInstByPlaceId',
	GetInitClassData:baseUrl+'Entry/GetInitClassData',
	SmsSend: baseUrl+'Sms/SmsSend',
	OrderCreate:baseUrl+'Entry/OrderCreate',
};


/*
 * message常量
 */
var msg = {
	GLITZHOME: 'zzxc',
	NETWORK_IS_UNAVAILABLE: "网络连接失败，请检查您的网络.",
	LOAD_DATA_FAIL: '加载数据失败，请等一会再试!',
	AUTH_EXPIRED: '认证信息失效，请重新登录！',
	SERVER_ERROR: '服务响应失败！',
};

/*
 * APP版本号
 */
var curAppVer = "1.0.1";
/*
 * 全局的驾校编号
 */
//var areaCode=717000001;


/*
 * 工具类
 */
var tool = {
	isEmptyVal: function(val) {
		if(val === null || typeof(val) === 'undefined' || val === '')
			return true;

		return false;
	},
	isFunction: function(f) {
		return Object.prototype.toString.call(f) === "[object Function]";
	},
	isObject: function(o) {
		return Object.prototype.toString.call(o) === "[object Object]";
	},
	isString: function(s) {
		return Object.prototype.toString.call(s) === "[object String]";
	}
};

/* 
 * 保存数据
 * 注：目前使用h5的localStorage, 只能非跨域下工作
 */
function cache(key, val) {
	if(key === 'clear') {
		window.localStorage.clear();
	} else if(val === null) {
		window.localStorage.removeItem(key);
	} else if(val) {
		window.localStorage[key] = val;
	} else {
		return window.localStorage[key];
	}
}

var animation = {
	popIn: 'pop-in',
	popOut: 'pop-out',
	slideInRight: 'slide-in-right',
	slideOutRight: 'slide-out-right',
	slideInBottom: 'slide-in-bottom',
	slideOutBottom: 'slide-out-bottom',
}

var app = {
	map: function(elements, callback) {
		var value, values = [],
			i, key, len;
		if(typeof elements.length === 'number') { //TODO 此处逻辑不严谨，可能会有Object:{a:'b',length:1}的情况未处理
			for(i = 0, len = elements.length; i < len; i++) {
				value = callback(elements[i], i);
				if(value != null) values.push(value);
			}
		} else {
			for(key in elements) {
				value = callback(elements[key], key);
				if(value != null) values.push(value);
			}
		}
		return values.length > 0 ? [].concat.apply([], values) : values;
	}
};

function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if(r != null) return unescape(r[2]);
	return null;
}

function numTozh(num) {
	switch(num) {
		case 0:
			return '零';
			break;

		case 1:
			return '一';
			break;
		case 2:
			return '二';
			break;
		case 3:
			return '三';
			break;
		case 4:
			return '四';
			break;
		case 5:
			return '五';
			break;
		case 6:
			return '六';
			break;
		case 7:
			return '七';
			break;
		case 8:
			return '八';
			break;
		case 9:
			return '九';
			break;
		default:
			return '';
			break;
	}
}

function getStarTxt(val) {
	if(val == 1)
		return "很差";
	else if(val == 2)
		return "较差";
	else if(val == 3)
		return "一般";
	else if(val == 4)
		return "良好"
	else if(val == 5)
		return "优秀";
}

/*
 * 封装常用的打开窗口，一般只需传入url及id
 * 默认显示等待框，默认不自动显示
 */
function openPage(url, id, extraObj, styleObj, showObj, waitObj, extrasObj) {
	showObj = showObj || {
		autoShow: true
	};
	waitObj = waitObj || {
		autoShwo: true
	}; // 默认显示等待框

	extrasObj = extrasObj || {};

	mui.openWindow({
		url: url,
		id: id,
		styles: styleObj,
		extras: extraObj,
		show: showObj,
		waiting: waitObj
	});
}

function showPage(wv, aniShow, closeWaiting, duration) {
	if(tool.isEmptyVal(wv)) {
		console.log('webview为空');
		return;
	}
	aniShow = aniShow || animation.popIn;
	duration = duration || (mui.os.ios ? 300 : 250); // pop-in动画在ios平台上300ms为佳，在android平台上250为佳
	wv.show(aniShow, duration);
	closeWaiting = typeof(closeWaiting) === "undefined" ? true : closeWaiting; // 默认要关闭
	//if(closeWaiting) plus.nativeUI.closeWaiting();
}

/*
 *
 * 判断数组是否包含某元素
 */
function arrayContains(a, obj) {
	var i = a.length;
	while(i--) {
		if(a[i] === obj) {
			return true;
		}
	}
	return false;
}

/*
 * 数据访问
 */
var persistent = {
	idcard: function() {
		return cache('idcard');
	},
	areacode: function() {
		return cache('areacode');
	},
	uid: function() {
		return cache('uid');
	},
	unionid: function() {
		return cache('unionid');
	}
}

var ajaxStatus = {
	OK: 200,
	ERROR: 300,
	FAIL_AUTH: 400,
};

/*
 * 封装ajax
 */
var ajax = {
	getAuthHeader: function() {
		return persistent.uid() + ',' + persistent.utoken() + ',' + persistent.postcode() + ',' + 'sign';
	},
	post: function(url, paramObj, suc, wait, comp, err) {
		var sign = "";
		for(var param in paramObj) {
			sign += param + "=" + paramObj[param] + "&";
		}
		var tick = parseInt(new Date().getTime() / 1000);
		sign += "r=" + tick;
		sign = md5(sign);
		// 检测网络是否可用
//		var network = plus.networkinfo.getCurrentType();
//		if(network === plus.networkinfo.CONNECTION_UNKNOW ||
//			network === plus.networkinfo.CONNECTION_NONE) {
//			mui.alert(msg.NETWORK_IS_UNAVAILABLE, msg.GLITZHOME, 'OK');
//			if(tool.isFunction(err)) err();
//			if(tool.isFunction(comp)) comp();
//			return;
//		}

		if(tool.isObject(url)) {
			paramObj = url.paramObj;
			suc = url.suc;
			wait = url.wait;
			comp = url.comp;
			err = url.err;
			url = url.url;
		}

		// 显示等待框
		// if(typeof(wait)==="undefined") wait=true; // 设置默认是否显示
//		wait = wait === null ? false : wait;
//		if(wait) {
//			plus.nativeUI.showWaiting();
//		}
		paramObj = paramObj || {};
		// uri编码
		for(var k in paramObj) {
			var v = paramObj[k];
			if(tool.isString(v)) {
				v = v.replace(/\\/g, '\\\\');
				paramObj[k] = encodeURIComponent(v);
			}
			// 只处理两层，应该够了
			else if(tool.isObject(v)) {
				for(var kk in v) {
					var vv = v[kk];
					if(tool.isString(vv)) {
						vv = vv.replace(/\\/g, '\\\\');
						v[kk] = encodeURIComponent(vv);
					}
				}
			}

		}

		var headerObj = {
			"sign": sign,
			"tick": tick,
			"cpid": "100001",
			"AreaCode": persistent.areacode()
		};
		/*		if(url !== api.Coach.login &&
					url !== api.system.checkforupdate) {
					paramObj.Authorization = this.getAuthHeader();
					//			headerObj.Authorization = this.getAuthHeader();
				}*/

		// 发送请求
		mui.ajax(url, {
			type: 'POST',
			timeout: 10000,
			data: paramObj,
			dataType: 'json',
			headers: headerObj,
			success: function(data, textStatus, xhr) {
				if(textStatus == 'success') {
					if(data.status != null && data.status && data.message != null && data.message.trim() != "") {
						//plus.nativeUI.toast(data.message);
					}
					if(tool.isFunction(suc)) {
						//tonken过期
						if(data.status == "loginOut") {
							//plus.nativeUI.toast(data.message);
							//清空缓存
							persistent.clear();
							//plus.runtime.restart();
							//openPage('login.html', 'login');
						} else {
							suc(data, textStatus, xhr);
						}
					}
				} else {
					var errMsg = data.msg || msg.LOAD_DATA_FAIL;
					//plus.nativeUI.toast(errMsg);
					//if (tool.isFunction(err)) err();
				}
				//				if (data.StatusCode === ajaxStatus.OK) {
				//					if (tool.isFunction(suc)) suc(data, textStatus, xhr);
				//				} else if (data.StatusCode === ajaxStatus.ERROR) {
				//					var errMsg = data.Msg || msg.LOAD_DATA_FAIL;
				//					plus.nativeUI.toast(errMsg);
				//					if (tool.isFunction(err)) err();
				//				} else if (data.StatusCode === ajaxStatus.FAIL_AUTH) {
				//					plus.nativeUI.toast(msg.AUTH_EXPIRED);
				//					mui.openWindow(plus.io.convertAbsoluteFileSystem('_www/login.html'), 'login', null);
				//					if (tool.isFunction(err)) err();
				//				} else {
				//					plus.nativeUI.toast(msg.SERVER_ERROR);
				//					if (tool.isFunction(err)) err();
				//				}
			},
			error: function(xhr, type) {
				console.log(type);
				//plus.nativeUI.toast(msg.LOAD_DATA_FAIL + "(" + type + ")");
				if(tool.isFunction(err)) err();
			},
			complete: function() {
				//if(wait) plus.nativeUI.closeWaiting();
				if(tool.isFunction(comp)) comp();
			}
		});
	},
	get: function(url, paramObj, suc, wait, comp, err) {
		var sign = "";
		for(var param in paramObj) {
			sign += param + "=" + paramObj[param] + "&";
		}
		var tick = parseInt(new Date().getTime() / 1000);
		sign += "r=" + tick;
		sign = md5(sign);
		// 检测网络是否可用
//		var network = plus.networkinfo.getCurrentType();
//		if(network === plus.networkinfo.CONNECTION_UNKNOW ||
//			network === plus.networkinfo.CONNECTION_NONE) {
//			mui.alert(msg.NETWORK_IS_UNAVAILABLE, msg.GLITZHOME, 'OK');
//			if(tool.isFunction(err)) err();
//			if(tool.isFunction(comp)) comp();
//			return;
//		}

		if(tool.isObject(url)) {
			paramObj = url.paramObj;
			suc = url.suc;
			wait = url.wait;
			comp = url.comp;
			err = url.err;
			url = url.url;
		}

		// 显示等待框
		// if(typeof(wait)==="undefined") wait=true; // 设置默认是否显示
//		wait = wait === null ? false : wait;
//		if(wait) {
//			plus.nativeUI.showWaiting();
//		}
		paramObj = paramObj || {};
		// uri编码
		for(var k in paramObj) {
			var v = paramObj[k];
			if(tool.isString(v)) {
				v = v.replace(/\\/g, '\\\\');
				paramObj[k] = encodeURIComponent(v);
			}
			// 只处理两层，应该够了
			else if(tool.isObject(v)) {
				for(var kk in v) {
					var vv = v[kk];
					if(tool.isString(vv)) {
						vv = vv.replace(/\\/g, '\\\\');
						v[kk] = encodeURIComponent(vv);
					}
				}
			}

		}

		var headerObj = {
			"sign": sign,
			"tick": tick,
			"cpid": "100001",
			"AreaCode": persistent.areacode()
				
		};
		/*		if(url !== api.Coach.login &&
					url !== api.system.checkforupdate) {
					paramObj.Authorization = this.getAuthHeader();
					//			headerObj.Authorization = this.getAuthHeader();
				}*/

		// 发送请求
		mui.ajax(url, {
			type: 'GET',
			timeout: 10000,
			data: paramObj,
			dataType: 'json',
			headers: headerObj,
			success: function(data, textStatus, xhr) {
				if(textStatus == 'success') {
					if(data.status != null && data.status && data.message != null && data.message.trim() != "") {
						//plus.nativeUI.toast(data.message);
					}
					if(tool.isFunction(suc)) {
						//tonken过期
						if(data.status == "loginOut") {
							//plus.nativeUI.toast(data.message);
							//清空缓存
							persistent.clear();
							//plus.runtime.restart();
							//openPage('login.html', 'login');
						} else {
							suc(data, textStatus, xhr);
						}
					}
				} else {
					var errMsg = data.msg || msg.LOAD_DATA_FAIL;
					//plus.nativeUI.toast(errMsg);
					//if (tool.isFunction(err)) err();
				}
				//				if (data.StatusCode === ajaxStatus.OK) {
				//					if (tool.isFunction(suc)) suc(data, textStatus, xhr);
				//				} else if (data.StatusCode === ajaxStatus.ERROR) {
				//					var errMsg = data.Msg || msg.LOAD_DATA_FAIL;
				//					plus.nativeUI.toast(errMsg);
				//					if (tool.isFunction(err)) err();
				//				} else if (data.StatusCode === ajaxStatus.FAIL_AUTH) {
				//					plus.nativeUI.toast(msg.AUTH_EXPIRED);
				//					mui.openWindow(plus.io.convertAbsoluteFileSystem('_www/login.html'), 'login', null);
				//					if (tool.isFunction(err)) err();
				//				} else {
				//					plus.nativeUI.toast(msg.SERVER_ERROR);
				//					if (tool.isFunction(err)) err();
				//				}
			},
			error: function(xhr, type) {
				console.log(type);
				//plus.nativeUI.toast(msg.LOAD_DATA_FAIL + "(" + type + ")");
				if(tool.isFunction(err)) err();
			},
			complete: function() {
				//if(wait) plus.nativeUI.closeWaiting();
				if(tool.isFunction(comp)) comp();
			}
		});
	}
};
/*
 * 处理url参数函数
 */
var UrlParm = function () { // url参数
    var data, index;
    (function init() {
        data = [];
        index = {};
        var u = window.location.search.substr(1);
        if (u != '') {
            var parms = decodeURIComponent(u).split('&');
            for (var i = 0, len = parms.length; i < len; i++) {
                if (parms[i] != '') {
                    var p = parms[i].split("=");
                    if (p.length == 1 || (p.length == 2 && p[1] == '')) {
                        data.push(['']);
                        index[p[0]] = data.length - 1;
                    } else if (typeof (p[0]) == 'undefined' || p[0] == '') {
                        data[0] = [p[1]];
                    } else if (typeof (index[p[0]]) == 'undefined') { // c=aaa
                        data.push([p[1]]);
                        index[p[0]] = data.length - 1;
                    } else {// c=aaa
                        data[index[p[0]]].push(p[1]);
                    }
                }
            }
        }
    })();
    return {
        //window.location.search.substr(1)
        urlParm: function () {
            try { return window.location.search.substr(1); }
            catch (e){}
        },
        // 获得参数,类似request.getParameter()
        parm: function (o) { // o: 参数名或者参数次序
            try {
                return (typeof (o) == 'number' ? data[o][0] : data[index[o]][0]);
            } catch (e) {
            }
        },
        //获得参数组, 类似request.getParameterValues()
        parmValues: function (o) { //  o: 参数名或者参数次序
            try {
                return (typeof (o) == 'number' ? data[o] : data[index[o]]);
            } catch (e) { }
        },
        //是否含有parmName参数
        hasParm: function (parmName) {
            return typeof (parmName) == 'string' ? typeof (index[parmName]) != 'undefined' : false;
        },
        // 获得参数Map ,类似request.getParameterMap()
        parmMap: function () {
            var map = {};
            try {
                for (var p in index) { map[p] = data[index[p]]; }
            } catch (e) { }
            return map;
        }
    }
}();


var Entry={
	GetHome:function(suc, err) {
		ajax.get(apiEntry.GetHome, null,
			function(data, textStatus, xhr) {
				suc(data);
			}, true, null,
			function(xhr, type) {
				err(xhr);
			}); 
	},
	GetInitInfo:function(suc, err) {
		ajax.get(apiEntry.GetInitInfo, null,
			function(data, textStatus, xhr) {
				suc(data);
			}, true, null,
			function(xhr, type) {
				err(xhr);
			}); 
	},
	GetInitClass:function(take,suc,err){
		var params = {};
		params.take = take;
		ajax.get(apiEntry.GetInitClass, params,
			function(data, textStatus, xhr) {
				suc(data);
			}, true, null,
			function(xhr, type) {
				err(xhr);
			}); 
	},
	GetInstByPlaceList:function(suc, err) {
		var params = {};
		params.pageSize = 0;
		params.pageIndex = 0;
		ajax.get(apiEntry.GetInstByPlaceList, params,
			function(data, textStatus, xhr) {
				suc(data);
			}, true, null,
			function(xhr, type) {
				err(xhr);
			});
	},
	GetInstByPlaceId:function(placeId,suc, err) {
		var params = {};
		params.placeId = placeId;
		ajax.get(apiEntry.GetInstByPlaceId, params,
			function(data, textStatus, xhr) {
				suc(data);
			}, true, null,
			function(xhr, type) {
				err(xhr);
			});
	},
	smsSend: function(phoneNum, suc, err) {
		var params = {};
		params.recNum = phoneNum;
		ajax.post(apiEntry.SmsSend, params,
			function(data, textStatus, xhr) {
				console.log(data);
				suc(data);
			}, true, null,
			function(xhr, type) {
				err(xhr);
			});
	},
	GetInitClassData:function(ClassID,suc, err) {
		var params = {};
		params.ClassID = ClassID;
		ajax.get(apiEntry.GetInitClassData, params,
			function(data, textStatus, xhr) {
				suc(data);
			}, true, null,
			function(xhr, type) {
				err(xhr);
			});
	},
	OrderCreate: function(params, suc, err) {
		ajax.post(apiEntry.OrderCreate, params,
			function(data, textStatus, xhr) {
				console.log(data);
				suc(data);
			}, true, null,
			function(xhr, type) {
				err(xhr);
			});
	},
}


//添加用户详细信息页相关方法及属性
