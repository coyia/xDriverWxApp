(function($, doc) {	
	$.init({

    });

    $.ready(function () {
    		var PlaceId=0;
			if(UrlParm.hasParm("placeId"))
			{
				PlaceId=UrlParm.parmValues("placeId")[0];
			}
			Entry.GetInstByPlaceId(PlaceId,function(data) {
				if(data.status) {
					
					document.getElementById("ShowPlace_Img").src=data.data.instPlace.MasterPlaceImageUrl==null?"/XDriver/Content/images/no-pic.jpg":data.data.ImgHost+data.data.instPlace.MasterPlaceImageUrl;
					document.getElementById("Show_Name").innerHTML=data.data.instPlace.Name;
					document.getElementById("Show_Address").innerHTML=data.data.instPlace.Address;
					document.getElementById("Show_DrvLicenseArgs").innerHTML=data.data.instPlace.DrvLicenseArgs;
					document.getElementById("Show_ContactTel").innerHTML=data.data.instPlace.ContactTel;
					document.getElementById("Show_ContactBy").innerHTML=data.data.instPlace.ContactBy;
					document.getElementById("Show_Area").innerHTML=data.data.instPlace.Area+"平方米";
					document.getElementById("Show_CoachCount1").innerHTML=data.data.instPlace.CoachCount;
					document.getElementById("Show_CoachCount2").innerHTML=data.data.instPlace.CoachCount+"人";
					document.getElementById("Show_Introduction").innerHTML=data.data.instPlace.Introduction;
					
					var imgs="";
					for (var i=0;i<data.data.PlaceImageUrlArgs.UrlRows.length;i++) {
						imgs+="<img src='"+data.data.ImgHost+data.data.PlaceImageUrlArgs.UrlRows[i].Url+"'/>";
					}
					document.getElementById("Show_Imgs").innerHTML=imgs;
				} else {
					mui.alert(data.message, null);
				}
			},
			function(data) {
				mui.alert('服务器维护中，请稍后再试！', null);
			});
			

    });
	
}(mui, document));  