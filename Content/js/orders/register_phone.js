$(document).ready(function() {
	var getCodeBtn = document.getElementById('getCodeBtn'); //获取动态密码按钮
	var loginBtn = document.getElementById('loginBtn'); //登录按钮
	var phoneNumTxt = document.getElementById('phoneNumTxt'); //手机号输入框
	var codeTxt = document.getElementById('codeTxt'); //动态密码框

	getCodeBtn.addEventListener('click', function(event) {
		var phoneNum = phoneNumTxt.value;
		console.log(phoneNum);
		if(currentTime > -1) {
			mui.alert('短信已发到您的手机，请稍后重试!', null);
		} else if(!checkPhoneNum(phoneNum)) {
			mui.alert('请输入正确的手机号码！', null);
		} else {
			System.smsSend(phoneNum,
				function(data) {
					if(data.status) {
						currentTime = maxTime;
						interval = setInterval(function() {
							currentTime--;
							getCodeBtn.innerText = "获取动态密码" + (currentTime > 0 ? "(" + currentTime + ")" : "");
							if(currentTime <= 0) {
								currentTime = -1
								clearInterval(interval)
							}
						}, 1000)
					} else {
						mui.alert(data.message, null);
					}
				},
				function(data) {
					mui.alert('服务器维护中，请稍后再试！', null);
				});
		}
	});

	loginBtn.addEventListener('click', function(event) {
		var recNum = phoneNumTxt.value;
		var smsCode = codeTxt.value;
		System.login(recNum, smsCode,
			function(data) {
				if(data.status) {
					mui.back();
				} else {
					mui.alert(data.message, null);
				}
			},
			function(data) {
				mui.alert('服务器维护中，请稍后再试！', null);
			});
	});

});
var phoneNum;
var maxTime = 60;
var currentTime = -1; //倒计时的事件（单位：s）  
// 检测是否有输入  
function checkIsNotNull(content) {
	return(content && content != null)
}

// 检测输入内容  
function checkPhoneNum(phoneNum) {
	if(!checkIsNotNull(phoneNum)) {
		return false
	} else if(phoneNum.length != 11) {

		return false;
	}

	return true
}