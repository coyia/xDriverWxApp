$(document).ready(function() {
	var userStatus = 0;
	System.getUserStatus(
		function(data) {
			userStatus = data.data.userStatus;
			console.log(userStatus);
			switch(userStatus) {
				case 0:
					openPage("register_phone.html", "register_phone");
					break;
				case 1:
					openPage("register_detail.html", "register_detail");
					break;
				case 2:
					break;
			}
		},
		function(xhr) {});
});